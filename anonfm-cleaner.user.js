// ==UserScript==
// @name         anonfm-cleaner
// @namespace    http://tampermonkey.net/
// @version      1.0.9
// @description  Cleans up sayings of toxic people
// @author       vitoyucepi
// @license      AGPL-3.0-or-later
// @match        *://anon.fm/info.html*
// @run-at       document-end
// ==/UserScript==

/**
 * Adds script styles
 */
function addStyles() {
    const style = document.createElement('style');
    document.head.appendChild(style);
    style.sheet.insertRule('.toggleInput { display: none; }');
    style.sheet.insertRule('.toggleContent { display: none; }');
    style.sheet.insertRule('.toggleLabel { cursor: pointer; }');
    style.sheet.insertRule('.toggleInput:checked + .toggleContent { display: initial }');
}

/**
 * Creates spoilers of user post
 * @param Element[] nodes
 */
function createSpoiler(nodes) {
    const replacementInput = document.createElement('input');
    const replacementLabel = document.createElement('label');
    const spoiler = document.createElement('span');
    const replacementId = Math.random().toString(36).substr(2, 9);

    replacementInput.classList.add('toggleInput');
    replacementLabel.classList.add('toggleLabel');
    spoiler.classList.add('toggleContent');

    replacementInput.id = `toggle-${replacementId}`;
    replacementLabel.htmlFor = `toggle-${replacementId}`;

    replacementInput.type = 'checkbox';
    replacementLabel.textContent = '[этот текст может нанести вред вашему психическому здоровью]';

    nodes[0].before(replacementLabel, replacementInput, spoiler);
    spoiler.appendChild(document.createElement('br'));
    nodes.forEach(node => spoiler.appendChild(node));
}

/**
 * Selects user post nodes
 * @param Element node
 */
function selectSiblings(node) {
    node.classList.add('skip');
    const nodes = [];
    for(let sibling = node.nextSibling; !(sibling === null || sibling.classList !== undefined && sibling.classList.contains('user_id')); sibling = sibling.nextSibling) {
        if(sibling.classList !== undefined && sibling.classList.contains('timestamp')) {
            nodes.length = 0;
        } else if (sibling.classList !== undefined && sibling.classList.contains('userpost')) {
            nodes.length = 0;
            nodes.push(sibling);
        } else {
            nodes.push(sibling);
        }
    }
    if(nodes[nodes.length - 1].tagName === 'BR') {
        nodes.pop();
    }
    if(nodes[0].textContent === ' - ') {
        nodes.shift();
    }
    if(nodes[0].textContent.startsWith(': ')) {
        nodes[0].before(': ');
        nodes[0].textContent = nodes[0].textContent.slice(2);
    }
    if(nodes[0].textContent.startsWith(' - ')) {
        nodes[0].before(' - ');
        nodes[0].textContent = nodes[0].textContent.slice(3);
    }
    if(nodes.length === 1 && nodes[0].textContent === 'Расписание обновлено') {
        nodes.length = 0;
    }
    return nodes;
}

/**
 * Finds user posts
 * @param string user
 */
function findReplies(user) {
    return [...document.querySelectorAll('.user_id:not(.skip)')].filter(node=>node.textContent.includes(user));
}

/**
 * Hides posts of the users on page
 * @param string[] users
 */
function cleanPage(users) {
    const observer = new MutationObserver(
        () => users
        .map(findReplies)
        .flat()
        .map(selectSiblings)
        .filter(siblings => siblings.length > 0)
        .forEach(createSpoiler)
    );
    observer.observe(document.querySelector('#kookareque'), { childList: true });
}

const toxicUsers = [
    'Лебедева Ирина Олеговна',
    'Марченко Светлана Тимофеевна',
];

addStyles();
cleanPage(toxicUsers);
